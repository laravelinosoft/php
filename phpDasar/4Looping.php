<?php 

// For  Loop
    $total = 5;
    $hasilFaktorial = 1;

    for($i=1; $i<=$total; $i++){
        $hasilFaktorial = $hasilFaktorial * $i;
    }
    echo $hasilFaktorial."<br>";

// While
    $counter = 0;
    while($counter < 10 ){
        echo "<br> mulai dari ".$counter;
        $counter++;
    }

// Do While
    $angka = -2;
    do{
        $angka--;
    }while($angka > 0);
    echo "<br><br>angka = " .$angka. "<br>";

//For Each
    $makananFavorite = [
        "Budi" => "ayam",
        "Ayah" => "pecel",
        "Ibu" => "nasi padang",
        "Joko" => "Gado-gado",
        "Sari" => "Bakso"
    ];
    foreach ($makananFavorite as $key => $makanan){
        echo "<br>".$key." suka makan ".$makanan;
    }