<?php 

// if
    $kegiatan = "Pergi keluar";
    $hujan = true;
    if($hujan){
        $kegiatan = $kegiatan." membawa payung";
    }

    echo $kegiatan;

// if else
    $umur = 30;
    if($umur < 18){
        $hasil = "Belum cukup umur.";
    }else{
        $hasil = "Cukup umur.";
    }
    echo "<br>".$hasil;

// if - else if - else
    $angka = 33;
    if($angka == 0){
        $hasil = "Bilangan 0";
    }
    else if($angka % 2 == 0){
        $hasil = "Bilangan Genap";
    }
    else{
        $hasil = "Bilangan Ganjil";
    }
    echo "<br>".$hasil;

// nested if
    $cintaku = 13;
    $cintadia = 9;
    if($cintaku > $cintadia){
        $usahdia = 4;
        $cintadia = $cintadia + $usahdia;

        if($cintaku > $cintadia){
            $hasil = "Kamu Mencitaiku";
        }else{
            $hasil = "Kamu Mencintai dia";
        }
    }else{
        $hasil = "Kamu mencintai dia";
    }
    echo "<br>".$hasil;


// Switch Case
    $nilai = "C";
    switch($nilai){
        case "A":
            $hasil = "Sangat Baik";
            break;
        case "B":
            $hasil = "Baik";
            break;
        case "C":
            $hasil = "Cukup";
            break;
        case "D":
            $hasil = "Kurang";
            break;
        case "E":
            $hasil = "Sangat Kurang";
            break;
        default:
            $hasil = "Nilai Tidak Valid";
    }
    echo "<br>".$hasil;