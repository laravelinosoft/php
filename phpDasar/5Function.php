<?php 

// Function parameter
    function printHello(string $nama){
        echo "Halo ".$nama;
    }
    printHello("Sandika");

// Function
    function printHello2(String $nama, int $angka){
        echo "<br>Hallo ".$nama. ". Ini angka ".$angka;
    }
    printHello2("Dunia", 2);

// Default Argument
    function makan (bool $lapar = false){
        if($lapar){
            echo "<br>Ayo Makan";
        }else{
            echo "<br>Sudah Kenyang";
        }
    }
    makan();

// Return Value
    function menjumlahkanDuaAngka(int $angka1, int $angka2){
        $hasil = $angka1 + $angka2;
        return $hasil;
    }
    echo "<br>".menjumlahkanDuaAngka(45, 7);

// function dalam function
    function pembagianDuaAngka(int $angka1, int $angka2){
        $isZeroValue = cekNilaiNol($angka2);
        if($isZeroValue){
            return "Angka Kedua tidak boleh 0";
        }
        $hasil = $angka1 / $angka2;
        return $hasil;
    }
    function cekNilaiNol(int $angka){
        if($angka == 0){
            return true;
        }
        else return false;
    }
    echo "<br>".pembagianDuaAngka(44, 9);
