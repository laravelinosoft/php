<?php 

// array
    $makananFavorite = ["ayam", "pecel"];
    $minumanFavorite = array("teh", "kopi");

    foreach($makananFavorite as $makananku){
        echo "$makananku, ";
    }
    echo "<br>";
    foreach($minumanFavorite as $minumanku){
        echo "$minumanku, ";
    }

// Indexing Array
    $makanan = ["ayam", "pecel", "sate"];
    echo "<br>Saya suka makan ". $makanan[0]." dan ".$makanan[2];

    
// Assosiative Array
    $makananFavorite = ["Budi"=>"ayam", "ayah"=>"pecel", "sate"];
    echo "<br>Saya suka makan ". $makananFavorite["Budi"]." dan ayah suka makan ".$makananFavorite["ayah"];

// Multidimensional Array
    $dataStatus = [
        ["Budi", 12, "jomblo"],
        ["Joko", 24, "menikah"]
    ];
    echo "<br>".$dataStatus[0][0]. " berumur ".$dataStatus[0][1]." berstatus ".$dataStatus[0][2];
    echo "<br>".$dataStatus[1][0]. " berumur ".$dataStatus[1][1]." berstatus ".$dataStatus[1][2];
