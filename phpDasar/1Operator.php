<?php

echo "Operator Aritmatika ";
    $pertambahan = 10 + 10;
    var_dump($pertambahan);

    $pengurangan = 10 - 5;
    var_dump($pengurangan);

    $modulus = 9 % 2;
    var_dump($modulus);

    $pangkat = 3**2;
    var_dump($pangkat);


echo "<br>"."<br>"."Operator Penugasan Aritmatika ";
    $total = 0;

    $apel = 10000;
    $ayam = 35000;
    $airMineral = 5000;

    $total += $apel;
    $total += $ayam;
    $total += $airMineral;

    var_dump($total);


echo "<br>"."<br>"."operator Perbandingan ";
    var_dump("10" == 10);   //true
    var_dump("10" === 10);  //true

    var_dump(10 > 9);       //true
    var_dump(10 >= 10);     //true

    
echo "<br>"."<br>"."Operator Logika ";
    var_dump(true && true); //true
    var_dump(true && false); //false

    var_dump(true || false); //true
    var_dump(false || false); //false

    var_dump(!false); //true
    var_dump(!true); //false

    

    
echo "<br>"."<br>"."Operator Increment & Decrement ";
    $a = 10;
    $b = ++$a;
    //sama dengan 
    // $a = $a + 1; //11
    // $b = $a; //11

    var_dump($b);
    var_dump($a);

    $c= 10;
    $d = $c++;
    //sama dengan
    // $d = $c; //10
    // $c = $c+1; //11

    var_dump($d);
    var_dump($c);