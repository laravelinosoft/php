<?php 

// Parent Class
class Kalkulator2{

    public float $dayaBaterai;      // Atribute

    public function __construct(float $dayaBaterai){
        $this->dayaBaterai = $dayaBaterai;
    }

    public function tambah(int $a, int $b){
        return $a + $b;
    }
}

// Child Class
class KalkulatorMewah extends Kalkulator2{

}

$kalkulator2 = new Kalkulator2(77);     // Object
// echo $kalkulator -> dayaBaterai;
echo $kalkulator2->tambah(5, 19);