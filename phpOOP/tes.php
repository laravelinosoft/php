<?php 

// class Produk {
// 	public  $judul = "judul", 
// 			$penulis = "penulis", 
// 			$penerbit = "penerbit",
// 			$harga = 0;

// 	public function getLabel(){
// 		return "$this->penulis, $this->penerbit";
// 	}
// }

// $produk3 = new Produk();
// $produk3->judul = "Naruto";
// $produk3->penulis = "Masasi Kishimoto";
// $produk3->penerbit = "Shonen Jump";
// $produk3->harga = 30000;

// $produk4 = new Produk();
// $produk4->judul= "Uncharted";
// $produk4->penulis= "Neil Druckman";
// $produk4->penerbit = "Sony Computer";
// $produk4->harga = 250000;

// echo "Komik : ". $produk3->getLabel();
// echo "<br>";
// echo "Game : ". $produk4->getLabel();


class Produk {
	public  $judul, $penulis, $penerbit, $harga;

	public function __construct($judul="", $penulis = "", $penerbit = "", $harga = 0){
		$this->judul = $judul;
		$this->penulis = $penulis;
		$this->penerbit = $penerbit;
		$this->harga = $harga;
	}

	public function getLabel(){
		return "$this->judul, $this->penerbit";
	}
}


$produk1 = new Produk("Naruto", "Masasi Kishimoto", "Shonen Jump", 30000);
$produk2 = new Produk("Uncharted", "Neil Druckman", "Sony Computer", 250000);
$produk3 = new Produk("Clash of Clan", "","Super Cell");

echo "Komik : ". $produk1->getLabel();
echo "<br>";
echo "Game : ". $produk2->getLabel();
echo "<br>";
echo "Game : ". $produk3->getLabel();